// Fill out your copyright notice in the Description page of Project Settings.

#include "TestCollectionObject.h"
#include "BehavioralPattern/Iterator/Iterators/TestIterator.h"

UIterator* UTestCollectionObject::CreateFriendsIterator()
{
	auto Iterator = NewObject<UTestIterator>();

	if(Iterator)
		Iterator->SetCollection(this);

	return Iterator;
}
