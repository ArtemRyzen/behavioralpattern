// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Collection/TestCollectionObject.h"
#include "GameFramework/GameModeBase.h"
#include "Iterators/TestIterator.h"
#include "Kismet/KismetSystemLibrary.h"
#include "IteratorGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API AIteratorGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	UTestCollectionObject* TestCollectionObject;

	virtual void BeginPlay() override;
};

inline void AIteratorGameMode::BeginPlay()
{
	Super::BeginPlay();

	TestCollectionObject = NewObject<UTestCollectionObject>();
    	
    auto TestIterator = Cast<UTestIterator>(TestCollectionObject->CreateFriendsIterator());

    //Fill
    if(TestIterator)
    {
        TestIterator->AddObject(NewObject<UTestCollectionObject>());
        TestIterator->AddObject(NewObject<UTestCollectionObject>());
        TestIterator->AddObject(NewObject<UTestCollectionObject>());

        while(TestIterator->HasMore())
        {
            auto TestObject = Cast<UTestCollectionObject>(TestIterator->GetNext());
            UKismetSystemLibrary::PrintString(this, !TestObject ? "Null" : TestObject->GetName()); 
        }
    }
}

