// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Iterator.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API UIterator : public UObject
{
	GENERATED_BODY()

public:
	virtual class UCollectionObject* GetNext() { return nullptr; }
	virtual bool HasMore() { return false; }
};
