// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "GameSaver.generated.h"

class AItem;
class AMyPlayer;

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API UGameSaver : public USaveGame
{
	GENERATED_BODY()

public:
	static UGameSaver* Get();
	static void SaveGame();
	static void LoadGame();

	void SaveItem(AItem* Item);
	void SavePlayer(AMyPlayer* Player);

private:
	static UGameSaver* Instance;

	UPROPERTY()
	FName ItemSkinName;

	UPROPERTY()
	FName PlayerName;

	UPROPERTY()
	float PlayerHP;
};

UGameSaver* UGameSaver::Instance = nullptr;