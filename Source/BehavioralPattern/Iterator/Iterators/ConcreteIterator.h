// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Iterator.h"
#include "BehavioralPattern/Iterator/Collection/ConcreteCollectionObject.h"
#include "UObject/NoExportTypes.h"
#include "ConcreteIterator.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API UConcreteIterator : public UIterator
{
	GENERATED_BODY()

public:
	void SetCollection(UConcreteCollectionObject* CollectionObject)
	{
		ConcreteCollection = CollectionObject;
	};

	virtual UCollectionObject* GetNext() override;

	virtual bool HasMore() override;
	
private:
	UPROPERTY()
	class UConcreteCollectionObject* ConcreteCollection;

	int32 CurrentPosition;

	UPROPERTY()
	TArray<UConcreteCollectionObject*> Cache;
};

inline UCollectionObject* UConcreteIterator::GetNext()
{
	if(HasMore())
	{
		auto Object = Cast<UConcreteCollectionObject>(Cache[CurrentPosition]);
		CurrentPosition++;
		return Object;
	}

	return nullptr;
}

inline bool UConcreteIterator::HasMore()
{
	return CurrentPosition < Cache.Num();
}