// Copyright Epic Games, Inc. All Rights Reserved.

#include "BehavioralPattern.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BehavioralPattern, "BehavioralPattern" );
