// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayer.h"
#include "GameSaver.h"

void AMyPlayer::SaveData_Implementation(UGameSaver* Saver)
{
	if(Saver)
	{
		Saver->SavePlayer(this);
	}
}
